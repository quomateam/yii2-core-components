<?php

use yii\db\Migration;

/**
 * Class m201111_145001_menu_item_label_255_chars
 */
class m201111_145001_menu_item_label_255_chars extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('menu_item', 'label', 'VARCHAR(255) NOT NULL');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('menu_item', 'label', 'VARCHAR(45) NOT NULL');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201111_145001_menu_item_label_255_chars cannot be reverted.\n";

        return false;
    }
    */
}
